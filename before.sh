if [[ ! -d "venv" ]]; then
  mkdir venv
  virtualenv venv
fi

source ./venv/bin/activate
pip3 install -r requirements.txt
pytest