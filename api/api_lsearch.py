import requests

import api
from loguru import logger


class ApiLsSearch:
    def __init__(self, area):
        sugg_addr = "/local-service/api/suggestions"
        self.host = api.host.get(area)
        self.querysugg_url = self.host + sugg_addr

    def api_lssearch_querysugg(self, city_id, keyword):
        params = {"city_id": city_id, "keyword": keyword}
        logger.info(f"url为：{self.querysugg_url}请求参数为：{params}")
        return requests.get(self.querysugg_url, params=params)





