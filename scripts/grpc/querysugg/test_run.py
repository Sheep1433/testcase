
import pytest

from scripts.grpc.querysugg.proto import o2oalgo_querysugg_pb2_grpc,o2oalgo_querysugg_pb2
import grpc
from loguru import logger
from google.protobuf.internal.containers import RepeatedCompositeFieldContainer
args = {
  "user_id": 24,
  "query": "Te",
  "scene": 0,
  "ab_test": "dsa=xxx,ls.suggestion_dict=test2",
  "city_id": 51,
  "publish_id": "15e412f8-a462-43a2-8f12-99d93c919a0f"
}

addr = '10.103.112.15:8080'

logger.add("logs/grpc_{time}.log")


class TestRun:
    @pytest.mark.skip
    # 自己服务器ping不到公司
    def test_run(self):
        with grpc.insecure_channel(addr) as channel:
            stub = o2oalgo_querysugg_pb2_grpc.O2OalgoQuerysuggStub(channel)
            resp: o2oalgo_querysugg_pb2.GetQuerySuggestListResp = stub.GetLsQuerySuggestList(o2oalgo_querysugg_pb2.GetLsQuerySuggestListReq(user_id=args["user_id"], \
                                                                                             query=args["query"], scene=args["scene"], ab_test=args["ab_test"], city_id=args["city_id"], publish_id=args["publish_id"]))
            logger.info(f"{resp.query_suggest_list}")
            logger.info(type(resp.query_suggest_list))
            logger.info(type(resp))

            assert resp is None


