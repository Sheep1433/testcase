# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: microkit.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import descriptor_pb2 as google_dot_protobuf_dot_descriptor__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0emicrokit.proto\x12\x08microkit\x1a google/protobuf/descriptor.proto:6\n\x0bProjectName\x12\x1f.google.protobuf.ServiceOptions\x18\xd2\x86\x03 \x01(\t')


PROJECTNAME_FIELD_NUMBER = 50002
ProjectName = DESCRIPTOR.extensions_by_name['ProjectName']

if _descriptor._USE_C_DESCRIPTORS == False:
  google_dot_protobuf_dot_descriptor__pb2.ServiceOptions.RegisterExtension(ProjectName)

  DESCRIPTOR._options = None
# @@protoc_insertion_point(module_scope)
