import pytest

from api.api_lsearch import ApiLsSearch
from loguru import logger
from tools.read_yaml import read_yaml


class TestLsSearchQuerySugg:
    def setup_class(self):
        self.apilssearch = ApiLsSearch("id")

    # 能够匹配，分别包含部分匹配，完整匹配和两个单词组合的匹配模式
    @pytest.mark.parametrize("city_id, keyword", read_yaml("lssearchquerysugg.yml"))
    def test_matchright(self, city_id, keyword):
        r = self.apilssearch.api_lssearch_querysugg(city_id, keyword)
        json_data = r.json()
        assert json_data is not None
        logger.info(f"响应结果为：{json_data}")
        assert 0 == json_data.get("err_code")
        assert "success" == json_data.get("err_msg")
        datas = json_data.get("data")
        total = datas.get("total")
        data = datas.get("data")
        assert len(data) == total
        assert total > 0
