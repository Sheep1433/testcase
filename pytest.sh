current_data=$(date "+%Y%m%d%H%M%S")
pytest -s ./testcase --alluredir target/allure-results/${current_data} 2>&1 | tee run_test.log
allure generate target/allure-results/${current_data} -o ./reports/ --clean
#allure open ./reports/
result_code=$?
echo "result_code=$result_code" > res.tmp
