import pytest
import grpc
from loguru import logger

from pb2 import o2oalgo_querysugg_pb2, o2oalgo_querysugg_pb2_grpc

addr = "10.103.162.170:8080"
args = {
  "user_id": 24,
  "query": "Te",
  "scene": 0,
  "ab_test": "dsa=xxx,ls.suggestion_dict=test2",
  "city_id": 51,
  "publish_id": "15e412f8-a462-43a2-8f12-99d93c919a0f"
}


class TestLsSearchQuerySugg:
    @pytest.mark.skip
    def test_lsearchquerysugg(self):
        with grpc.insecure_channel(addr) as channel:
            stub = o2oalgo_querysugg_pb2_grpc.O2OalgoQuerysuggStub(channel)
            resp: o2oalgo_querysugg_pb2.GetQuerySuggestListResp = stub.GetLsQuerySuggestList(o2oalgo_querysugg_pb2.GetLsQuerySuggestListReq(user_id=args["user_id"], \
                                                                                             query=args["query"], scene=args["scene"], ab_test=args["ab_test"], city_id=args["city_id"], publish_id=args["publish_id"]))
            logger.info(f"{resp.query_suggest_list}")
            logger.info(type(resp.query_suggest_list))
            logger.info(type(resp))

            assert resp is not None

    @pytest.mark.parametrize("nums", [1,2,3,4,5,7])
    def test_num(self, nums):
        assert nums > 0
        assert nums < 10

    @pytest.mark.parametrize("nums", [4,11])
    def test_n(self, nums):
        assert nums > 0
        assert nums < 10